La web de maleteo esta en:
(sólo se puede acceder a maleteo con usuario logeado y sesión activa)
http://localhost:8000/maleteo

A través de esta url podemos visualizar las personas que solicitaron una DEMO:
http://localhost:8000/mostrarDemos

A través de este formulario podemos insertar opiniones en la DB:
http://localhost:8000/insertarOpinion

A través de esta página podemos ver el listado de opiniones que están en la DB:
http://localhost:8000/mostrarOpinion

A través de esta página me registro como usuario para poder acceder al LOGIN y ver demo maleteo:
http://localhost:8000/registro

A través de esta página veo los datos de todos los usuarios registrados que pueden ver la demo:
http://localhost:8000/mostrarRegistros

A través de esta página puedes hacer Login con tu usuario registrado y acceder a Maleteo index:
http://localhost:8000/login



