<?php

namespace App\Repository;

use App\Entity\UsuarioDemo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UsuarioDemo|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsuarioDemo|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsuarioDemo[]    findAll()
 * @method UsuarioDemo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

 
class UsuarioDemoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsuarioDemo::class);
    }

    public function findName($email, $contra){
        return $this->createQueryBuilder('u')
        ->andWhere('u.email = :email and u.contra= :contra')
        ->setParameter('email', $email)
        ->setParameter('contra', $contra)
        ->getQuery()
        ->getResult()
    ;
   
}

public function findTodos(){
    $em = $this->getEntityManager();
    $dql = "SELECT user FROM App\Entity\UsuarioDemo user";
    $query = $em->createQuery($dql);
    return $query->execute();

}

    // /**
    //  * @return UsuarioDemo[] Returns an array of UsuarioDemo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsuarioDemo
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
